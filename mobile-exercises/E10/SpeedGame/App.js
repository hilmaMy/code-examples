import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import { Header, Icon, Button } from 'react-native-elements';
import Dialog from "react-native-dialog";
import Realm from 'realm';


/* const Player = {
  name: 'Player',
  propertier: {
    name: 'string',
    score: {type: 'int', default: 0},
  },
};

const realm = new Realm({schema: [Player]}); */

export default function App() {
  const [visible, setVisible] = useState(false);
  const [name, setName] = useState("");
  const [score, setScore] = useState(0);
  const [timeOne, setTime] = useState(0);
  const [players, setPlayers] = useState([
    {
    name: 'Tiuhti',
    score: '888'
    },
    {
      name: 'Viuhti',
      score: '1024'
    },
    {
      name: 'Vauhti',
      score: '5462'
    }

  ]);
  const [index, setIndex] = useState(0);

 
  

  

  const indexChange = (index) => {
    setIndex(index);
    /* if (index === 1) {
      let players = realm.objects('Player').sorted('score');
      let playersArray = Array.from(players);
      setPlayers(playersArray);
    } */
  }

  const showDialog = () => {
    setVisible(true);
  }
  
  const okClicked = () => {
    setVisible(false);
    /* realm.write(() => {
      const player = realm.create('Player', {
        name: name,
        score: score,
      });
    }); */
  }

  const resetGame = () => {
    setScore(0);
    setTime(0);
  }
  
  const circlePressed = () => {
    // starting on the first press
    if (timeOne === 0) {
      const date = new Date();
      setTime(date.getTime());
      setScore(0);
    // ending on the second press
    } else {
      const date = new Date();
      setScore(date.getTime() - timeOne);
    } 
  }

  const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#fff' }]}>
      <Text style={styles.text}>Double tap the circle as fast as you can!</Text>
      <View style={styles.circle} onTouchStart={circlePressed}/>
      <Text style={styles.text}>Time: {score}</Text>
      <View style={styles.row}>
        <View style={styles.button}>
          <Button  title="Add highscores" onPress={showDialog} />
        </View>
        <View style={styles.button}>
          <Button  title="Reset time" onPress={resetGame}/>
        </View>
      </View>
    </View>
  );
  
  const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#fff'}]}>
      <ScrollView>
        {players.map((player, index) => {
          return (
            <View key={index} style={styles.highscore}>
              <Text style={styles.highscoreName}>{player.name}</Text>
              <Text style={styles.highscoreScore}>{player.score}</Text>
            </View>
          )
        })}
      </ScrollView>
    </View>
  );

  const initialLayout =  Dimensions.get('window').width;

  const [routes] = React.useState([
    { key: 'first', title: 'Game' },
    { key: 'second', title: 'Highscores' },
  ]);
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  return (
    <>
      <Header  
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'SPEED GAME', style: { color: '#fff' } }} />
      <TabView navigationState={{ index, routes }} renderScene={renderScene} onIndexChange={indexChange} initialLayout={initialLayout} />
      <Dialog.Container visible={visible}>
        <Dialog.Title>Add a new highscore</Dialog.Title>
        <Dialog.Input label="Name" placeholder="Click and type your name here" onChangeText={text => setName(text)}/>
        <Dialog.Button label="Ok" onPress={okClicked} />
      </Dialog.Container>
    </>
  );
};

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  circle: {
    width: 150,
    height: 150,
    borderRadius: 150/2,
    backgroundColor: 'red',
    alignSelf : "center",
    marginTop: 100
  },
  text: {
    marginTop: 50,
    alignSelf : "center"
  },
  button: {
    marginRight: 20,
    marginTop: 50,
    alignSelf : "center",
    width: 150
  },
  row: {
    flexDirection: 'row',
    alignSelf : "center"
  },
  highscore: {
    flexDirection: 'row',
    margin: 10,
  },
  highscoreName: {
    fontSize: 20,
    color: 'black',
    width: '50%',
    textAlign: 'right',
    marginRight: 5
  },
  highscoreScore: {
    fontSize: 20,
    color: 'gray',
    width: '50%',
    marginLeft: 5
  }
});
