

import React, { useState, useEffect } from 'react';
import { Container, Header, Body, Title, Left, Right, Button, Text, Form, Item, Input, Card, CardItem, Content} from 'native-base';
import Dialog from 'react-native-dialog';
import useAxios from 'axios-hooks';
import AsyncStorage from '@react-native-async-storage/async-storage';

const WeatherForecast = (params) => {
  const city = params.city;
  const API_KEY = ' ';
  const URL = 'https://api.openweathermap.org/data/2.5/weather?q=';

  const [{data, loading, error}, refetch] = useAxios(
    URL+city+'&units=metric&appid='+API_KEY
  )

  if (loading) return <Text>Loading...</Text>
  if (error) return <Text>Error!</Text>

  const refreshForecast = () => {
    refetch();
  }

  const deleteCity = () => {
    params.deleteCity(params.id);
  }

  console.log(data);
  return (
    <Card>
      <CardItem>
        <Body>
          <Text>{data.name}, {data.sys.country}</Text>
          <Text>Main: {data.weather[0].main}</Text>
          <Text>Temp: {data.main.temp} °C, Feels like: {data.main.feels_like} °C</Text>
          <Text onPress={deleteCity}>Delete</Text>
          <Text onPress={refreshForecast}>Refresh</Text>
        </Body>
      </CardItem>
    </Card>
  );
}

const App: () => React$Node = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [cityName, setCityName] = useState("");
  const [cities, setCities] = useState([]);

  const openDialog = () => {
    setModalVisible(true);
  }

  const addCity = () => {
    setCities([...cities,{id:cities.length, name:cityName}]);
    setModalVisible(false);
  }

  const cancelCity = () => {
    setModalVisible(false);
  }

  const deleteCity = (id) => {
    let filteredArray = cities.filter(city => city.id !== id);
    setCities(filteredArray);
  }

  const storeData = async () => {
    try {
      await AsyncStorage.setItem('@cities', JSON.stringify(cities));
    } catch (e) {
      console.log("Cities saving error!")
    }
  }

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@cities')
      if(value !== null) {
        setCities(JSON.parse(value));0
      }
    } catch(e) {
      console.log("Cities loading error!");
    }
  }

  useEffect(() => {
    getData();
  },[]);

  useEffect(() => {
    storeData();
  },[cities]);

  return (
    <>
      <Container>
  <Header>
    <Left/>
    <Body>
      <Title>Weather App</Title>
    </Body>
    <Right>
      <Button>
        <Text onPress={openDialog}>Add</Text>
      </Button>
    </Right>
  </Header>
  <Content>
    {!modalVisible && cities.map(function(city,index){
      return (
      <WeatherForecast 
        key={index} 
        city={city.name} 
        id={city.id} 
        deleteCity={deleteCity} />
      );
    })}
  </Content>
  <Dialog.Container visible={modalVisible}>
    <Dialog.Title>Add a new city</Dialog.Title>
    <Form>
      <Item>
        <Input onChangeText={(text) => setCityName(text)} placeholder="cityname"/>
      </Item>
    </Form>
    <Dialog.Button label="Cancel" onPress={cancelCity} />
    <Dialog.Button label="Add" onPress={addCity}/>
  </Dialog.Container>
</Container>
    </>
  );
};



export default App;
