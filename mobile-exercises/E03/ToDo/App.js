import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View, ScrollView, TextInput, Button, Keyboard } from 'react-native';

class Banner extends React.Component {
  render() {
    return (
      <View style={styles.banner}>
        <Text style={styles.bannerText}>ToDo-list</Text>
      </View>
    );
  }
}

class ToDoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {items: [], text: ''};
  }

  addToDoItem = () => {
    if (this.state.text !== '') {
      this.setState({
        items: [...this.state.items, this.state.text],
        text: ''
      });
      Keyboard.dismiss();
    }
  }

  deleteToDoItem = (index) => {
    var items = this.state.items;
    items.splice(index, 1);
    this.setState({items:items});
  }
  
  render() {
    var items = this.state.items.map(function(item, index){
    return (
      <ListItem text={item} key={index} index={index} deleteToDoItem={this.deleteToDoItem}/>
    )
    }.bind(this));

    return(
    <View>
      <View style={styles.addTodo}>
        <TextInput style={styles.AddToDoTextInput} onChangeText={(text) => this.setState({text})} value={this.state.text} />
        <Button title="Add" style={styles.addTodoButton} onPress={this.addToDoItem} />
      </View>
      <ScrollView style={styles.list}>
        {items}
      </ScrollView>
    </View>
    );
  }
}

class ListItem extends React.Component {
  deleteToDoItem = (index) => {
    this.props.deleteToDoItem(index);
  }
  
  render() {
    return (
      <View style={styles.listItem}>
        <Text style={styles.listItemText}>* {this.props.text}</Text>
        <Text style={styles.listItemDelete} onPress={(e) => this.deleteToDoItem(this.props.index)}>X</Text>
      </View>
    )
  }   
}

export default function App() {
  return(
    <View style={styles.container}>
      <Banner />
      <ToDoList />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 5
  },
  banner: {
    backgroundColor: 'cadetblue',
    justifyContent: 'center',
    marginBottom: 20
  },
  bannerText: {
    color: '#ffffff',
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 20
  },
  addTodo: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  AddToDoTextInput: {
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#ccc',
    padding: 5,
    margin: 2,
    flex: 1,
  },
  list: {
    color: 'black',
    margin: 2,
  },
  listItem: {
    flex: 1,
    flexDirection: 'row',
    margin: 5
  },
  listItemText: {
  },
  listItemDelete: {
    marginStart: 10,
    color: 'red',
    fontWeight: 'bold'
  } 
});
