import React, { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import "firebase/firestore";
import { Card, Button, TextField, Select, MenuItem, FormControl, InputLabel, makeStyles } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles({
  app: {
    backgroundColor: '#74708e',
    width: "100%",
    height: 10000,
  },
  item: {
    backgroundColor: '#3b394a',
    minWidth: "20%",
    padding: 15,
    boxShadow: 30,
    borderRadius: 10,
    margin: 5,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    backgroundColor: "#323040",
    margin: 10
  },
  addCard: {
    backgroundColor: '#74708e',
  },
  add: {
    minWidth: "20%",
    margin: 5
  },
  count: {
    minWidth: "10%",
    margin: 3
  },
})

const firebaseConfig = {
  apiKey: " ",
  authDomain: "shoppinglist-42c5c.firebaseapp.com",
  projectId: "shoppinglist-42c5c",
  storageBucket: "shoppinglist-42c5c.appspot.com",
  messagingSenderId: " ",
  appId: " "
}


function App() {
  const [loading, setLoading] = useState(true);
  const [items, setItems] = useState("");
  const[item, setItem] = useState("");
  const[count, setCount] = useState(1);
  

  const classes = useStyles();

  useEffect(() => {
    const fetchData = async () => {
      firebase.initializeApp(firebaseConfig);
      const db = firebase.firestore();
      const data = await db.collection("items").get();

      const items = data.docs.map(doc => {
        return {
          name: doc.data().name,
          count: doc.data().count,
          id: doc.id
        };
      });
      setItems(items);
      setLoading(false);
    }
    fetchData();
  },[]);

  const addItem = async () => {
    let newItem = { name: item, count: count, id: ''};
    const db = firebase.firestore();
    let doc = await db.collection('items').add(newItem);
    newItem.id = doc.id;
    setItems( [...items,newItem]);
    setItem("");
    setCount(1);
  }

  const deleteItem = async (item) => {
    const db = firebase.firestore();
    db.collection('items').doc(item.id).delete();
    let filteredArray = items.filter(collectionItem => collectionItem.id !== item.id);
    setItems(filteredArray);
  }

  if (loading) return (<p>Loading...</p>);

  const sh_items = items.map((item, index) => {
    return (<Card key={index} className={classes.item}>{item.name} {item.count} <DeleteIcon onClick={() => deleteItem(item)}/></Card>)
  });

  return (
    <Card className= {classes.app}>
      <Card className={classes.addCard}>
        <TextField
          type="item"
          label="Item"
          variant="filled"
          className={classes.add}
          onChange={e => setItem(e.target.value)}
        />
        <FormControl className={classes.count}>
          <InputLabel>Count</InputLabel>
          <Select onChange={e => setCount(e.target.value)} >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
            <MenuItem value={4}>4</MenuItem>
            <MenuItem value={5}>5</MenuItem>
          </Select>
        </FormControl>
        <Button variant="contained" className={classes.button} onClick={() => addItem()}>
          Add
        </Button>
      </Card>
      {sh_items}
    </Card>
  );
}

export default App;
