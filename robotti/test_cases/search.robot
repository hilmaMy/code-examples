*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot
Suite Setup       Open Browser To Search Page
Suite Teardown    Close Browser

*** Test Cases ***
Valid Search By City
    Input Search    Toivakka
    Submit Search
    City Should Be Open

Valid Search By Destination Type
    Input Search  Luontopolku
    Submit Search
    Type Should Be Open

Valid Search By Letter
    Input Search  r
    Submit Search
    Letter Should Be Open

Invalid Search
    Input Search  Toivakkka
    Submit Search
    City Should Be Open

Invalid Open Settings
    Open Settings

Valid SideBar Toggler testing
    Toggler Click    
    [Teardown]    Close Browser