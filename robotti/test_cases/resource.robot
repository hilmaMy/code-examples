*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           OperatingSystem


*** Variables ***
${SERVER}         193.167.189.207
${BROWSER}        firefox
${DELAY}          1
${SEARCH_KEY}    mode
${SEARCH URL}      http://${SERVER}/

*** Keywords ***
Open Browser To Search Page
    Open Browser    ${SEARCH URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Search Page Should Be Open

Search Page Should Be Open
    Title Should Be    React App

Go To Search Page
    Go To    ${SEARCH URL}
    Search Page Should Be Open

Input Search
    [Arguments]    ${search_key}
    Input Text   id=search    ${search_key}

Submit Search
    Press Keys  search  RETURN

City Should Be Open
    Location Should Be    ${SEARCH URL}
    Element Text Should Be    id=507715  Koulukeskuksen kuntosali

Type Should Be Open
    Location Should Be    ${SEARCH URL}
    Element Text Should Be    id=96615  Kuutinharjun luontopolku

Letter Should Be Open
    Location Should Be      ${SEARCH URL}
    Element Text Should Be      id=90017  Keilahalli Siilinhohto/Fontanella

Search Shou
    
Open Settings
    Click Button    settings

Toggler Click 
    Click Element   toggler



