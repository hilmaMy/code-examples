import React, { useRef } from "react";
import { Typography, Box, Button, TextField } from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";
import axios from "axios";
import { API_URL } from "./../config";
import { useStyles } from "../styles/styles";

/**
 * Register a new user
 */

const Register = () => {
  const classes = useStyles();
  const { handleSubmit, errors: fieldsErrors, control, watch } = useForm();
  const password = useRef({});
  const minPasswordLenght = 6;
  const email = useRef({});
  email.current = watch("email", "");
  password.current = watch("password", "");

  // send user data to backend
  const postData = async (data) => {
    console.log(data.email);
    axios({
      method: "post",
      url: `${API_URL}/users`,
      data: {
        user_firstname: data.firstName,
        user_lastname: data.lastName,
        user_email: data.email,
        user_password: data.password,
      },
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        if (res.data === 201) {
          // If successful
          console.log("Successful user creation");
          window.location.href = "/";
        } else {
          console.log(`Something went wrong.`);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Box className={classes.components}>
      <Typography gutterBottom className={classes.mainTitle}>
        Register
      </Typography>
      {/* Registration form */}
      <form onSubmit={handleSubmit(postData)}>
        <Box className={classes.inputBox}>
          <Controller
            name="firstName"
            defaultValue=""
            as={
              <TextField
                label="First name"
                type="text"
                className={classes.input}
                helperText={
                  fieldsErrors.firstName ? fieldsErrors.firstName.message : null
                }
                error={fieldsErrors.firstName}
              />
            }
            control={control}
            rules={{
              required: "Required",
            }}
          />
        </Box>
        <Box className={classes.inputBox}>
          <Controller
            name="lastName"
            defaultValue=""
            as={
              <TextField
                label="Last name"
                type="text"
                className={classes.input}
                helperText={
                  fieldsErrors.lastName ? fieldsErrors.lastName.message : null
                }
                error={fieldsErrors.lastName}
              />
            }
            control={control}
            rules={{
              required: "Required",
            }}
          />
        </Box>

        <Box className={classes.inputBox}>
          <Controller
            name="email"
            defaultValue=""
            as={
              <TextField
                label="Email"
                type="text"
                className={classes.input}
                helperText={
                  fieldsErrors.email ? fieldsErrors.email.message : null
                }
                error={fieldsErrors.email}
              />
            }
            control={control}
            rules={{
              required: "Required",
              pattern: {
                value: /^\S+@\S+$/i,
                message: "Incorrect email",
              },
            }}
          />
        </Box>

        <Box className={classes.inputBox}>
          <Controller
            name="confirm_email"
            defaultValue=""
            as={
              <TextField
                label="Confirm email"
                type="text"
                className={classes.input}
                helperText={
                  fieldsErrors.confirm_email
                    ? fieldsErrors.confirm_email.message
                    : null
                }
                error={fieldsErrors.confirm_email}
              />
            }
            control={control}
            rules={{
              required: "Required",
              validate: (value) =>
                value === email.current || "Emails don't match",
            }}
          />
        </Box>

        <Box className={classes.inputBox}>
          <Controller
            name="password"
            defaultValue=""
            as={
              <TextField
                label="Password"
                type="password"
                className={classes.input}
                helperText={
                  fieldsErrors.password ? fieldsErrors.password.message : null
                }
                error={fieldsErrors.password}
              />
            }
            control={control}
            rules={{
              required: "Password required",
              minLength: {
                value: minPasswordLenght,
                message: `Password must have at least ${minPasswordLenght} characters`,
              },
            }}
          />
        </Box>
        <Box className={classes.inputBox}>
          <Controller
            name="password_repeat"
            defaultValue=""
            as={
              <TextField
                label="Confirm password"
                type="password"
                className={classes.input}
                helperText={
                  fieldsErrors.password_repeat
                    ? fieldsErrors.password_repeat.message
                    : null
                }
                error={fieldsErrors.password_repeat}
              />
            }
            control={control}
            rules={{
              required: "Password required",
              validate: (value) =>
                value === password.current || "Passwords don't match",
            }}
          />
        </Box>
        <Button
          variant="contained"
          className={classes.submitButton}
          type="submit"
        >
          Register
        </Button>
      </form>
    </Box>
  );
};

export default Register;
