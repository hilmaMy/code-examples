﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class BombSpawner : MonoBehaviour
{
    public Tilemap tilemap;

    public GameObject bombPrefab;
    public GameObject player1;
    public GameObject player2;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("enter"))
        {
            Vector3Int cell = tilemap.WorldToCell(player1.transform.position);
            Vector3 cellCenterPros = tilemap.GetCellCenterWorld(cell);

            Instantiate(bombPrefab, cellCenterPros, Quaternion.identity);

        }

        if (Input.GetKeyDown("space"))
        {
            Vector3Int cell = tilemap.WorldToCell(player2.transform.position);
            Vector3 cellCenterPros = tilemap.GetCellCenterWorld(cell);

            Instantiate(bombPrefab, cellCenterPros, Quaternion.identity);
        }
    }
}
