﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public Transform movePoint;

    public LayerMask whatStopsMovement;

    public string HorizontalAxis = "Horizontal";
    public string VerticalAxis = "Vertical";

    private float boostTimer;
    private bool boosting;

    void Start()
    {
        movePoint.parent = null;
        moveSpeed = 4f;
        boostTimer = 0;
        boosting = false;
    }

    // Update is called once per frame
    void Update()
    {

            transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, movePoint.position) <= .05f)
        {
            if (Mathf.Abs(Input.GetAxisRaw(HorizontalAxis)) == 1f)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(Input.GetAxisRaw(HorizontalAxis), 0f, 0f), .2f, whatStopsMovement))
                {
                    movePoint.position += new Vector3(Input.GetAxisRaw(HorizontalAxis), 0f, 0f);
                }
            }
            else if (Mathf.Abs(Input.GetAxisRaw(VerticalAxis)) == 1f)
            {
                if (!Physics2D.OverlapCircle(movePoint.position + new Vector3(Input.GetAxisRaw(HorizontalAxis), 0f, 0f), .2f, whatStopsMovement))
                {
                    movePoint.position += new Vector3(0f, Input.GetAxisRaw(VerticalAxis), 0f);
                }
            }
        }

        if (boosting)
        {
            boostTimer += Time.deltaTime;
            if(boostTimer >= 10f)
            {
                moveSpeed = 5f;
                boostTimer = 0;
                boosting = false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "SpeedBoost")
        {
            boosting = true;
            moveSpeed = 10f;
            Destroy(other.gameObject);
        }
    }

}
