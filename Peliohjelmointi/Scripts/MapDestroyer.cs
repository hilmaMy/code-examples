﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;

public class MapDestroyer : MonoBehaviour
{
    public Tilemap tilemap;

    public Tile wallTile;
    public Tile destructibleTile;

    public GameObject explosionPrefab;

    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private int count;

    void Start()
    {
        count = 0;
        SetCountText();

        winTextObject.SetActive(false);
    }

    public void Explode (Vector2 wolrdPos)
    {
        Vector3Int originCell = tilemap.WorldToCell(wolrdPos);

        ExplodeCell(originCell);
        ExplodeCell(originCell + new Vector3Int(1, 0, 0));
        ExplodeCell(originCell + new Vector3Int(2, 0, 0));
        ExplodeCell(originCell + new Vector3Int(0, 1, 0));
        ExplodeCell(originCell + new Vector3Int(0, 2, 0));
        ExplodeCell(originCell + new Vector3Int(-1, 0, 0));
        ExplodeCell(originCell + new Vector3Int(-2, 0, 0));
        ExplodeCell(originCell + new Vector3Int(0, -1, 0));
        ExplodeCell(originCell + new Vector3Int(0, -2, 0));
    }

    bool ExplodeCell (Vector3Int cell)
    {
        Tile tile = tilemap.GetTile<Tile>(cell);
        
        if (tile == wallTile)
        {
            return false;
        }

        if (tile == destructibleTile)
        {
            // Remove tile
            tilemap.SetTile(cell, null);
            count = count + 1;
            SetCountText();
        }

        Vector3 pos = tilemap.GetCellCenterWorld(cell);
        Instantiate(explosionPrefab, pos, Quaternion.identity);

        return true;

    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();

        if (count >= 38)
        {
            winTextObject.SetActive(true);
        }
    }

}
