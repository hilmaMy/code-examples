# Code Examples


## Peliohjelmointi

Yksinkertainen peliprojekti peliohjelmoinnin kurssille. Toteutin projektin C#.

## Mobile-exercises

Mobiiliohjelmoinnin kurssin harjoituksia, tehty javaScriptillä ja React Nativella

## React_examples

Esimerkki Pull My Data - projektin koodista ja pari kuvaa käyttöliittymästä.

## Robotti

Robot Frameworkilla tehtyjä testicaseja.

